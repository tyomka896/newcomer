class Matrix:
    """
    Class Matrix that could be just a row
    or an array with the same number of rows and columns
    """
    def __init__(self, matrix):
        if not isinstance(matrix, list):
            raise TypeError("Parameter should be list type")

        if isinstance(matrix[0], list):
            length = len(matrix[0])
            for m in matrix:
                if len(m) != length:
                    raise Exception("Matrix should has the same number of rows and columns")
            self.__size = (len(matrix), length)
        else:
            self.__size = (len(matrix), )

        self.matrix = matrix

    def __add__(self, other):
        return Algebra.add(self, other)

    def __sub__(self, other):
        return Algebra.sub(self, other)

    def __mul__(self, other):
        return Algebra.mul(self, other)

    def __getitem__(self, item):
        return self.matrix[item]

    def __str__(self):
        return str(self.list)

    def __len__(self):
        count = 0
        for q in self.matrix:
            count += 1
        return count

    def __iter__(self):
        for q in self.matrix:
            yield q

    @property
    def list(self):
        """
        :return: list type of class
        """
        return [q for q in self]

    @property
    def demention(self):
        """
        :return: rows and columns of matrix
        """
        return self.__size

    @property
    def transpose(self):
        return Algebra.transpose(self.matrix, self.__size)


class Algebra:
    def __init__(self):
        pass

    # ИЗБАВИТЬСЯ ОТ *ARGS И ВЫПОЛНЯТЬ ОПЕРАЦИИ С ДВУМЯ ВЫДАННЫМИ МАТРИЦАМИ

    @classmethod
    def add(cls, left, right):
        if not isinstance(left, Matrix) and not isinstance(right, Matrix):
            raise TypeError("Arguments should be Matrix class")

        if left.demention != right.demention:
            raise Exception("Matrices should have the same number of rows and columns for auditioning\n"
                            f"You are trying to add {left} with {right}")

        new_matrix = []
        length = left.demention

        if len(length) == 1:
            for ind, val in enumerate(left):
                new_matrix.append(right[ind] + val)
        elif len(length) == 2:
            for vL, vR in zip(left, right):
                row = []
                for l, r in zip(vL, vR):
                    row.append(l + r)
                new_matrix.append(row)

        return Matrix(new_matrix)

    @classmethod
    def sub(cls, left, right):
        if not isinstance(left, Matrix) and not isinstance(right, Matrix):
            raise TypeError("Arguments should be Matrix class")

        if left.demention != right.demention:
            raise Exception("Matrices should have the same number of rows and columns for subtraction\n"
                            f"You are trying to sub {left} with {right}")

        new_matrix = []
        length = left.demention

        if len(length) == 1:
            for ind, val in enumerate(left):
                new_matrix.append(right[ind] - val)
        elif len(length) == 2:
            for vL, vR in zip(left, right):
                row = []
                for l, r in zip(vL, vR):
                    row.append(l - r)
                new_matrix.append(row)

        return Matrix(new_matrix)

    @classmethod
    def mul(cls, left, right):
        def mul_on_number(matrix, number):
            length = matrix.demention

            if len(length) == 1:
                return Matrix([q * number for q in matrix])
            if len(length) == 2:
                return Matrix([[q * number for q in w] for w in matrix])

        if (not isinstance(left, Matrix) and not isinstance(left, int)) or \
           (not isinstance(right, Matrix) and not isinstance(right, int)):
            raise TypeError("Arguments should be Matrix class or int type")

        if isinstance(left, int):
            return mul_on_number(right, left)
        if isinstance(right, int):
            return mul_on_number(left, right)

        if left.demention != right.transpose.demention:
            raise Exception("Matrices should have number of rows equal to number of columns "
                            "or vice versa for multiplication\n"
                            f"You are trying to mul {left} with {right}")

        right = right.transpose
        new_matrix = []

        for indL, rowL in enumerate(left):
            row = []
            for indR, rowR in enumerate(right):
                val = 0
                for l, r in zip(rowL, rowR):
                    val += l * r
                row.append(val)
            new_matrix.append(row)

        return Matrix(new_matrix)

    @classmethod
    def transpose(cls, matrix, size):
        new_matrix = []

        if len(size) == 1:
            for q in matrix:
                new_matrix.append([q])
        elif len(size) == 2:
            row, col = size
            new_matrix = [[matrix[w][q] for w in range(row)] for q in range(col)]

        return Matrix(new_matrix)


if __name__ == "__main__":
    arr1 = [[0, 1, 2], [3, 4, 5], ]
    arr2 = [[3, 2, 1], [3, 2, 1], ]
    arr3 = [[5, 7, 9], [6, 8, 10], ]
    arr4 = [[4, 5], [9, 7], [6, 1], ]

    new_arr = Matrix(arr1) * Matrix(arr4)

    print(f"demention of the new matrix is {new_arr.demention}")
    print(f"matrix is {new_arr}")
    print(f"transposed matrix is {new_arr.transpose}")
