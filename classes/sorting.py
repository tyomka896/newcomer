import random as rnd
import time as tm


# [method, array, algorithm_name]
def print_array(m, a, al):
    start_time = tm.time()
    m(a)

    print(f"- {al} Sort Algorithm " + "(%.9f ms)" % (tm.time() - start_time))
    # print(f"{a}")


def array_type(tp):
    arr_to_sort = []

    if tp == 1:
        # Small array
        arr_to_sort = [4, 2, 7, 5, 6, 8, 3, 1]
    elif tp == 2:
        # Grouped numbers in array
        arr_to_sort = [4 for n in range(1000)] + [1 for n in range(1000)] + [5 for n in range(1000)] + [2 for n in range(1000)] + [3 for n in range(1000)]
    elif tp == 3:
        # Sorted array with two numbers replaced
        arr_to_sort = [n for n in range(1000)]
        arr_to_sort[555], arr_to_sort[999] = arr_to_sort[999], arr_to_sort[555]
    elif tp == 4:
        # Reversed sorted array
        arr_to_sort = [n for n in reversed(range(10000))]
    elif tp == 5:
        # Array with random numbers
        arr_to_sort = [rnd.randrange(0, 1000) for _ in range(10000)]
    else:
        arr_to_sort = [0]

    return arr_to_sort


def do_all_tests():
    for n in range(1, 6):
        arr_to_sort = array_type(n)
        print(arr_to_sort)

        print_array(bubble_sort_array, arr_to_sort.copy(), "Bubble")
        print_array(insert_sort_array, arr_to_sort.copy(), "Insert")
        print_array(select_sort_array, arr_to_sort.copy(), "Select")
        print_array(merge_sort_array, arr_to_sort.copy(), "Merge")

        print()


# Bubble Sort Implementation
def bubble_sort_array(a):
    for i in range(len(a)):
        for j in range(0, len(a) - i - 1):
            if a[j] > a[j + 1]:
                a[j], a[j + 1] = a[j + 1], a[j]

    return a


# Selection Sort Implementation
def select_sort_array(a):
    for i in range(len(a)):
        min_idx = i
        for j in range(i + 1, len(a)):
            if a[min_idx] > a[j]:
                min_idx = j

        a[i], a[min_idx] = a[min_idx], a[i]

    return a


# Insert Sort Implementation
def insert_sort_array(a):
    for n in range(len(a)):
        for m in range(n):
            if a[n] < a[m]:
                a[m], a[n] = a[n], a[m]

    return a


# Merge Sort Implementation
def merge_sort_array(a):
    def m_array(l, r):
        rst = []

        while len(l) > 0 and len(r) > 0:
            rst.append(l.pop(0) if l[0] < r[0] else r.pop(0))

        return rst + l + r

    if len(a) == 1:
        return a

    lt = merge_sort_array(a[0:len(a) // 2])
    rt = merge_sort_array(a[len(a) // 2:])

    return m_array(lt, rt)


if __name__ == "__main__":
    do_all_tests()
