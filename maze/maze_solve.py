import os
from PIL import Image, ImageDraw, ImageFont


class Node:
    def __init__(self, state, parent=None, action=None, dist=0, steps=0):
        """
        Represent node or a step
        :param state: current position
        :param parent: where from step was made
        :param action: what kind of move was made
        :param dist: distance already taken by the node (for MappingFrontier, look in solve() func)
        :param steps: steps current node made from start position
        """
        self.state = state
        self.parent = parent
        self.action = action
        self.dist = dist
        self.steps = steps


class _Frontier:
    def __init__(self):
        """
        Class defines the main sequence of moves
        and how to make them. In stack or queue mode.
        """
        self.frontier = []

    def add(self, node):
        self.frontier.append(node)

    def contains(self, state):
        return any(node.state == state for node in self.frontier)

    def isEmpty(self):
        return len(self.frontier) == 0


class StackFrontier(_Frontier):
    def next(self):
        """
        Selects next box in Stack data access mode.
        """
        if self.isEmpty():
            raise Exception("empty frontier")
        else:
            node = self.frontier[-1]
            self.frontier = self.frontier[:-1]
            return node


class QueueFrontier(_Frontier):
    def next(self):
        """
        Selects next box in Queue data access mode.
        """
        if self.isEmpty():
            raise Exception("empty frontier")
        else:
            node = self.frontier[0]
            self.frontier = self.frontier[1:]
            return node


class MappingFrontier(_Frontier):
    def next(self):
        """
        Regarding on distance map of the maze
        will choice the the shortest distance
        from possible ways
        """
        if self.isEmpty():
            raise Exception("empty frontier")
        else:
            minimum = self.frontier[0]

            for ft in self.frontier:
                if ft.dist < minimum.dist:
                    minimum = ft

            return self.frontier.pop(self.frontier.index(minimum))


class Maze:
    def __init__(self, file_name):
        """
        | Class maze specifies the structure and main from
        | of selected maze. It contains coordinates of
        | start position, target position and wall.
        """
        if not os.path.exists(file_name):
            raise Exception(f"File '{file_name}' not found in '{os.getcwd()}'")
        if not os.path.isfile(file_name):
            raise Exception(f"'{file_name}' is not a file")

        content = open(file_name, "r").read()

        if "A" not in content:
            raise Exception("Symbol A as a start point is not found")
        if "B" not in content:
            raise Exception("Symbol B as a goal point is not found")

        lines = content.splitlines()

        # Width and Height of the maze
        self.width = max(len(line) for line in lines)
        self.height = len(lines)

        # Solution for the maze
        self.solution = ()

        # Define walls in the maze
        self.walls = [[False for _ in range(self.width)] for _ in range(self.height)]

        for col_ind, line in enumerate(lines, start=0):
            for row_ind, ch in enumerate(line, start=0):
                if ch == "0":
                    self.walls[col_ind][row_ind] = True
                elif ch == "A":
                    self.start = (col_ind, row_ind)
                elif ch == "B":
                    self.target = (col_ind, row_ind)

        # Find distance for every box to target position
        self.distances = []

        for col_ind, col in enumerate(self.walls, start=0):
            for row_ind, row in enumerate(col, start=0):
                if self.walls[col_ind][row_ind]:
                    continue

                distance = abs(self.target[0] - col_ind) + abs(self.target[1] - row_ind)
                self.distances.append((distance, (col_ind, row_ind)))

    def __neighbors(self, state):
        """
        Selecting next actions (up, down, left, right)
        :param state: current position
        :return: next actions
        """
        row, col = state
        actions = (
            ("up", (row - 1, col)),
            ("down", (row + 1, col)),
            ("left", (row, col - 1)),
            ("right", (row, col + 1)),)

        result = []

        for action, (r, c) in actions:
            if 0 <= c < self.width and 0 <= r < self.height and not self.walls[r][c]:
                result.append((action, (r, c)))

        return result

    def solve(self, mode=""):
        """
        Finds the path from start position to target position
        with two different modes. Stack (StackFrontier)
        or Queue (QueueFrontier) mode.
        :param mode: mode to solve the maze 's' as BFS (StackFrontier)
                     'q' as DFS (QueueFrontier) and 'm' with distances (MappingFrontier)
        :return: Maze object
        """
        self.explored_count = 0
        self.explored = []

        node = Node(self.start)

        if mode == "s":
            front = StackFrontier()
        elif mode == "q":
            front = QueueFrontier()
        elif mode == "m":
            front = MappingFrontier()
        else:
            print(f"Wrong chosen mode '{mode}'")
            return self

        front.add(node)

        while True:
            if front.isEmpty():
                return self

            self.explored_count += 1
            node = front.next()

            if node.state == self.target:
                actions = []
                cells = []

                # Getting made action and cells way from each node
                while node.parent is not None:
                    actions.append(node.action)
                    cells.append(node.state)

                    node = node.parent

                # And reverse it for getting way from start to target
                actions.reverse()
                cells.reverse()

                self.solution = (actions, cells)
                return self

            self.explored.append(node.state)

            for action, state in self.__neighbors(node.state):
                if not front.contains(state) and state not in self.explored:
                    pos = [(d, p) for (d, p) in self.distances if p == state][0]

                    child = Node(state, parent=node, action=action, dist=(pos[0] + node.steps), steps=(node.steps + 1))

                    front.add(child)

        return self

    def create_image(self, file_name="maze", explored=False, mapping=False, open_after=False):
        """
        Creates and saves image of maze map
        :param file_name: name of the image
        :param explored: show explored paths
        :param mapping: show distances for every position to target position
        :param open_after: defines if to open image after creation is done
        :return: Maze object
        """
        if not self.walls:
            raise Exception("Maze structure not defined")

        img_name = file_name if file_name.strip() == "" else os.path.basename(file_name)
        outline = 5
        cube_size = 80

        width = len(self.walls[0]) * cube_size + (len(self.walls[0]) * outline + outline)
        height = len(self.walls) * cube_size + (len(self.walls) * outline + outline)

        sol = self.solution
        sol = sol[1] if sol else []

        img = Image.new('RGB', (width, height), (240, 240, 240))
        font = ImageFont.truetype("arial.ttf", 25)
        draw = ImageDraw.Draw(img)

        for row_ind, line in enumerate(self.walls, start=0):
            for col_ind, state in enumerate(line, start=0):
                xPos = outline * (col_ind + 1) + cube_size * col_ind
                yPos = outline * (row_ind + 1) + cube_size * row_ind

                if state:
                    color = (200, 200, 200)
                elif self.start == (row_ind, col_ind):
                    color = (124, 252, 0)
                elif self.target == (row_ind, col_ind):
                    color = (135, 206, 250)
                elif (row_ind, col_ind) in sol:
                    color = (240, 240, 0)
                elif explored and (row_ind, col_ind) in self.explored:
                    color = (255, 160, 122)
                else:
                    color = (255, 255, 255)

                draw.rectangle([(xPos, yPos), (xPos + cube_size, yPos + cube_size)], color)

                if mapping and self.distances:
                    pos = [(d, p) for (d, p) in self.distances if p == (row_ind, col_ind)]
                    pos = pos[0] if len(pos) > 0 else None

                    if pos and not self.start == (row_ind, col_ind) and not self.target == (row_ind, col_ind):
                        draw.text((xPos + 25, yPos + 25), f"{pos[0]}", (10, 10, 10), font)

        # Image name structure: [sqm] frontier mode -> [t] total steps -> [s] steps in final solution
        if len(sol) == 0:
            img.save(f"maps\\{img_name}.png")
        else:
            img.save(f"maps\\{img_name}.t{self.explored_count}.s{len(sol)}.png")

        if open_after:
            img.show()

        return self


if __name__ == "__main__":
    file = "maze.1"
    modes = "sqm"

    for mode in modes:
        Maze(f"maps\\{file}").solve(mode=mode).create_image(file_name=f"{file}.{mode}", explored=True)

    # Only crete image from file without solving
    Maze(f"maps\\{file}").solve(mode="").create_image(file_name=f"{file}", explored=True, mapping=True, open_after=True)
