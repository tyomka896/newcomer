import tkinter as tk
from random import randrange
from math import floor

# __name__ = "console_game"  # Starts the game in the console
__name__ = "gui_game"  # Starts the game in GUI window


class TicTacToe:
    def __init__(self, main_player=True, depth=0):
        """
        Tic tac toe game class.
        There is initialization of the hollow game map (map)
        and the winner in final state (win_place).
        :param main_player: defines if you wil make a first throw as X (tic) or you will throw as O (tac)
        :param depth: how deep in game tree to look next throw
        """
        self.main_player = main_player
        self.depth = 100 if depth == 0 else depth
        self.win_place = ""
        self.map = [[0 for _ in range(3)] for _ in range(3)]

    def show_status(self):
        """
        | Print the status of current game in console.
        """
        def map_table(val):
            if val == -1:
                return "O"
            elif val == 1:
                return "X"
            else:
                return " "

        table = [[map_table(val) for val in row] for row in self.map]

        print("State of the game: ")
        print(f" {table[0][0]} | {table[0][1]} | {table[0][2]}")
        print(f" {table[1][0]} | {table[1][1]} | {table[1][2]}")
        print(f" {table[2][0]} | {table[2][1]} | {table[2][2]}", end="\n\n")

    def make_throw(self, row, col):
        """
        Make a throw as a gamer with passed row and column.
        :param row: index of board row
        :param col: index of board column
        :return: True if throw successful and False if throw is not available or was already made
        """
        try:
            row, col = int(row), int(col)
        except ValueError:
            print("Invalid input\n")
            return False

        if not isinstance(row, int) or not isinstance(col, int):
            print("Only numbers allowed")
            return False

        if 0 > row or row > 2 or 0 > col or col > 2:
            print("Range should be between 0 and 2")
            return False

        # row, col = row - 1, col - 1

        if self.map[row][col] != 0:
            print(f"The throw [{row + 1}, {col + 1}] already was made\n")
            return False

        self.map[row][col] = 1 if self.main_player else -1

        return True

    def art_make_throw(self, throw=None):
        """
        AI trying to make Randomly its next throw.
        It is simple logic without thinking.
        Can pass your throw for testing fro AI (throw).
        :param throw: can pass your own throw for testing
        :return: True if throw successful and False if throw is not available or was already made
        """
        available = self.available(self.map)

        if not throw:
            rnd = randrange(0, len(available))
            row, col = available.pop(rnd)
        else:
            row, col = throw

        if self.map[row][col] != 0:
            print(f"The throw [{row + 1}, {col + 1}] already was made\n")
            return False

        self.map[row][col] = -1 if self.main_player else 1
        print(f"{'O' if self.main_player else 'X'} next throw is {row + 1} {col + 1}")

        return True

    def art_smart_throw(self):
        """
        | AI trying to make its 'smart' next throw
        | based on Minimax algorithm using recursion.
        """
        events = []
        natural = self.available(self.map)

        for ind, (row, col) in enumerate(natural):
            self.map[row][col] = -1 if self.main_player else 1
            events.append((ind, self.__art_make_predict(self.map, self.main_player, self.depth)))
            self.map[row][col] = 0

        answer = events[0]
        for (ind, event) in events:
            if answer[1] > event and self.main_player:
                answer = (ind, event)
            if answer[1] < event and not self.main_player:
                answer = (ind, event)

        answers = [(ind, val) for ind, val in events if val == answer[1]]
        answer = answers[randrange(0, len(answers))]

        row, col = natural.pop(answer[0])
        self.map[row][col] = -1 if self.main_player else 1

        print(f"{'O' if self.main_player else 'X'} next throw is {row + 1} {col + 1}")

        return row, col

    def __art_make_predict(self, status, is_maximize=True, depth=0):
        """
        Recursion function for finding the best next move for AI.
        :param status: current state or the map of the game
        :param is_maximize:  defines that we are trying to minimize or maximize our next throw
        :param depth: how deep in game tree we should descend
        :return: maximize or minimize value value depend on is_maximize
        """
        if self.has_winner(status) != 0:
            return self.has_winner(status)

        natural = self.available(status)
        if len(natural) == 0 or depth <= 0:
            return 0

        events = []

        if is_maximize:
            for (row, col) in natural:
                status[row][col] = 1 if self.main_player else -1
                events.append(self.__art_make_predict(status, False, depth - 1))
                status[row][col] = 0

            return max(events)
        else:
            for (row, col) in natural:
                status[row][col] = -1 if self.main_player else 1
                events.append(self.__art_make_predict(status, True, depth - 1))
                status[row][col] = 0

            return min(events)

    def has_winner(self, map):
        """
        Function find a winner in a passed map (map)
        :param map: find a winner in a passed map
        :return: number one if X (tic) is winner,
                 number minus one if O (tac) is winner
                or 0 (toe) if nobody wins.
        """
        sums = [map[0][0] + map[0][1] + map[0][2], map[1][0] + map[1][1] + map[1][2],
                map[2][0] + map[2][1] + map[2][2], map[0][0] + map[1][0] + map[2][0],
                map[0][1] + map[1][1] + map[2][1], map[0][2] + map[1][2] + map[2][2],
                map[0][0] + map[1][1] + map[2][2], map[0][2] + map[1][1] + map[2][0]]

        if any(c == -3 for c in sums):
            return -1
        elif any(c == 3 for c in sums):
            return 1
        else:
            return 0

    def define_winner(self):
        """
        | Function finds and defends the winner position.
        """
        def get_winner_draft(pos):
            if pos == 0:
                return "first row"
            elif pos == 1:
                return "second row"
            elif pos == 2:
                return "third row"
            elif pos == 3:
                return "first column"
            elif pos == 4:
                return "second column"
            elif pos == 5:
                return "third column"
            elif pos == 6:
                return "backslash"
            elif pos == 7:
                return "forward slash"
            else:
                return "not defined"

        sums = [self.map[0][0] + self.map[0][1] + self.map[0][2], self.map[1][0] + self.map[1][1] + self.map[1][2],
                self.map[2][0] + self.map[2][1] + self.map[2][2], self.map[0][0] + self.map[1][0] + self.map[2][0],
                self.map[0][1] + self.map[1][1] + self.map[2][1], self.map[0][2] + self.map[1][2] + self.map[2][2],
                self.map[0][0] + self.map[1][1] + self.map[2][2], self.map[0][2] + self.map[1][1] + self.map[2][0]]

        self.win_place = "Nobody wins"

        if any(c == -3 for c in sums):
            self.win_place = f"O is winner on {get_winner_draft(sums.index(-3))}"
            return True
        elif any(c == 3 for c in sums):
            self.win_place = f"X is winner on {get_winner_draft(sums.index(3))}"
            return True

        return False

    def available(self, map):
        """
        :param map: current state or map of the game
        :return: available moves in current state
        """
        natural = []
        for ir, row in enumerate(map):
            for ic, col in enumerate(row):
                if col == 0:
                    natural.append((ir, ic))

        return natural


class TicTacGame:
    def __init__(self):
        self.game = None

    def testing(self):
        """
        | Function for testing
        """
        self.game = TicTacToe()

        # GAME 1
        # self.game.make_throw(2, 2)
        # self.game.make_throw(1, 1)
        # self.game.make_throw(2, 3)
        # self.game.make_throw(3, 2)
        #
        # self.game.art_make_throw(throw=(0, 2))
        # self.game.art_make_throw(throw=(2, 2))
        # self.game.art_make_throw(throw=(1, 0))

        # GAME 2
        self.game.make_throw(1, 1)
        self.game.make_throw(2, 2)
        self.game.make_throw(2, 3)

        self.game.art_make_throw(throw=(0, 2))
        self.game.art_make_throw(throw=(2, 2))

        self.game.show_status()
        self.game.art_smart_throw(False)
        self.game.show_status()

    def start_game(self, main_player=True, smart=True, depth=0):
        """
        | Starter of the game.
        | Defends if you are playing as X (tic) (main_player) or O (tac)
        | and if AI will be smart (smart) (using art_smart_throw()) or not.
        """
        self.game = TicTacToe(main_player=main_player, depth=depth)

        while True:
            if self.game.define_winner():
                print(self.game.win_place)
                break

            if len(self.game.available(self.game.map)) == 0:
                print(self.game.win_place)
                break

            if main_player:
                row, col = input("Make your throw: ").split()

                if not self.game.make_throw(row, col):
                    continue
            else:
                if smart:
                    self.game.art_smart_throw()
                else:
                    self.game.art_make_throw()

            self.game.show_status()
            main_player = not main_player

        return True

    def start_game_only_AI(self, main_player=True, depth=0):
        """
        | Starter of the game with only AI players.
        | Defends if you are playing as X (tic) (main_player) or O (tac).
        """
        self.game = TicTacToe(main_player=main_player, depth=depth)
        maximize = not main_player

        while True:
            if self.game.define_winner():
                print(self.game.win_place)
                break

            if len(self.game.available(self.game.map)) == 0:
                print(self.game.win_place)
                break

            if main_player:
                self.game.art_smart_throw()
            else:
                self.game.art_smart_throw()

            self.game.show_status()
            main_player = not main_player

        return True


class TicTacGUI:
    def __init__(self, process, with_AI=True):
        """
        Defines the main parameters if the form be created
        :param process: logic of the game as TicTacToe class
        :param with_AI: playing with AI or in PvP mode
        """
        self.process = process
        self.with_AI = with_AI
        self.tic_throw = True

        self.root = tk.Tk()

        self.__form_width = 500
        self.__form_height = 550

        self.__canvas = None                          # Object of the main canvas
        self.__canvas_wh = 500                        # Width and height of the canvas
        self.__canvas_font = ("Comic Sans MS", 100)   # Font style of X (tic) and O (tac)
        self.__canvas_indent = 80                     # Indent of symbol placement
        self.__canvas_bg = "white"                    # Background color of the canvas
        self.__canvas_stripes = "black"               # Line color of game board
        self.__canvas_cursor = "left_ptr"             # Cursor style

        self.__btn = None                             # Button for style changing from AI to PvP and vice versa
        self.__lbl = None                             # Label to show status of the game

    def init(self):
        """
        Initialize the form, canvas and bind main events on elements
        :return: form that created
        """
        self.root.title("Tic Tac Toe game with AI")
        self.root.resizable(False, False)
        self.root.configure(bg=self.__canvas_bg)
        self.root.geometry(f"{self.__form_width}x{self.__form_height}+300+150")

        self.__canvas = tk.Canvas(self.root, width=self.__canvas_wh, height=self.__canvas_wh,
                                  background=self.__canvas_bg, cursor=self.__canvas_cursor, highlightthickness=0)

        if self.with_AI:
            self.__canvas.bind("<Button-1>", self.canvas_click)
        else:
            self.__canvas.bind("<Button-1>", self.canvas_pvp_click)

        self.__canvas.bind("<Button-3>", self.clear_canvas)
        self.__canvas.grid(row=0, column=0)

        self.__lbl = tk.Label(text="TIC THROW", bg="white", font=("Comic Sans MS", 20))
        self.__lbl.grid(row=1, column=0)

        self.__btn = tk.Button(self.root, text="PvP", height=1, bg="white", fg="gray", bd=0, font=("Comic Sans MS", 16))
        self.__btn.bind("<Button-1>", self.change_mode)
        self.__btn.place(x=self.__canvas_wh - 60, y=self.__canvas_wh)

        self.clear_canvas()

        return self.root

    def clear_canvas(self, event=None):
        self.process = TicTacToe()

        self.tic_throw = True
        self.__lbl.configure(text="TIC THROW", fg="black")
        self.__canvas.delete("all")

        wth = 2
        third = self.__canvas_wh / 3

        self.__canvas.create_rectangle(third - wth, 0, third + wth, self.__canvas_wh, fill=self.__canvas_stripes)
        self.__canvas.create_rectangle(third * 2 - wth, 0, third * 2 + wth, self.__canvas_wh, fill=self.__canvas_stripes)
        self.__canvas.create_rectangle(0, third - wth, self.__canvas_wh, third + wth, fill=self.__canvas_stripes)
        self.__canvas.create_rectangle(0, third * 2 - wth, self.__canvas_wh, third * 2 + wth, fill=self.__canvas_stripes)

    def canvas_click(self, event):
        print("canvas_click")
        if not self.__game_status():
            return False

        third = self.__canvas_wh / 3

        cell_x = floor(event.x / third)
        cell_y = floor(event.y / third)

        print(f"Cell is [{cell_x}, {cell_y}]")

        if not self.process.make_throw(cell_x, cell_y):
            return False

        pos_x, pos_y = self.__canvas_indent + third * cell_x, self.__canvas_indent + third * cell_y

        self.__canvas.create_text(pos_x, pos_y, font=self.__canvas_font, text="X", fill="black")
        self.__lbl.configure(text="TAC THROW", fg="black")

        self.__game_status()
        return self.AI_throw()

    def canvas_pvp_click(self, event):
        print("canvas_pvp_click")
        if not self.__game_status():
            return False

        third = self.__canvas_wh / 3

        cell_x = floor(event.x / third)
        cell_y = floor(event.y / third)

        print(f"Cell is [{cell_x}, {cell_y}]")

        if self.tic_throw:
            if not self.process.make_throw(cell_x, cell_y):
                return False
            symbol = "X"
            move = "TAC"
        else:
            if not self.process.art_make_throw((cell_x, cell_y)):
                return False
            symbol = "O"
            move = "TIC"

        self.tic_throw = not self.tic_throw

        pos_x, pos_y = self.__canvas_indent + third * cell_x, self.__canvas_indent + third * cell_y

        self.__canvas.create_text(pos_x, pos_y, font=self.__canvas_font, text=symbol, fill="black")
        self.__lbl.configure(text=f"{move} THROW", fg="black")

        return self.__game_status()

    def AI_throw(self):
        if not self.__game_status():
            return False

        row, col = self.process.art_smart_throw()

        third = self.__canvas_wh / 3
        pos_x, pos_y = self.__canvas_indent + third * row, self.__canvas_indent + third * col

        self.__canvas.create_text(pos_x, pos_y, font=self.__canvas_font, text="O", fill="black")
        self.__lbl.configure(text="TIC THROW", fg="black")

        return self.__game_status()

    def change_mode(self, event):
        self.process = TicTacToe()
        self.__lbl.configure(text="TIC THROW", fg="black")
        self.clear_canvas()

        if self.with_AI:
            self.__btn.configure(text="AI")
            self.__canvas.unbind("<Button-1>")
            self.__canvas.bind("<Button-1>", self.canvas_pvp_click)
        else:
            self.__btn.configure(text="PvP")
            self.__canvas.unbind("<Button-1>")
            self.__canvas.bind("<Button-1>", self.canvas_click)

        self.with_AI = not self.with_AI

    def __game_status(self):
        if self.process.define_winner():
            winner = self.process.has_winner(self.process.map)
            if winner == 1:
                self.__lbl.configure(text="TIC WON", fg="green")
                self.__show_winner(color="green")
                return False
            elif winner == -1:
                self.__lbl.configure(text="TAC WON", fg="red")
                self.__show_winner(color="red")
                return False

        if len(self.process.available(self.process.map)) == 0:
            self.__lbl.configure(text="TOE", fg="blue")
            return False

        return True

    def __show_winner(self, color):
        def draw_winner_line(pos):
            indent = 50
            third = self.__canvas_wh / 3

            if pos == 0:
                self.__canvas.create_line(third / 2, indent, third / 2 , self.__canvas_wh - indent, fill=color, width=5)
            elif pos == 1:
                self.__canvas.create_line(third * 2 - third / 2, indent, third * 2 - third / 2, self.__canvas_wh - indent, fill=color, width=5)
            elif pos == 2:
                self.__canvas.create_line(third * 3 - third / 2, indent, third * 3 - third / 2, self.__canvas_wh - indent, fill=color, width=5)
            elif pos == 3:
                self.__canvas.create_line(indent, third / 2, self.__canvas_wh - indent, third / 2, fill=color, width=5)
            elif pos == 4:
                self.__canvas.create_line(indent, third * 2 - third / 2, self.__canvas_wh - indent, third * 2 - third / 2, fill=color, width=5)
            elif pos == 5:
                self.__canvas.create_line(indent, third * 3 - third / 2, self.__canvas_wh - indent, third * 3 - third / 2, fill=color, width=5)
            elif pos == 6:
                self.__canvas.create_line(third / 2, third / 2, third * 3 - third / 2, self.__canvas_wh - third / 2, fill=color, width=5)
            elif pos == 7:
                self.__canvas.create_line(third * 3 - third / 2, third / 2, third / 2, self.__canvas_wh - third / 2, fill=color, width=5)

        map = self.process.map

        sums = [map[0][0] + map[0][1] + map[0][2], map[1][0] + map[1][1] + map[1][2],
                map[2][0] + map[2][1] + map[2][2], map[0][0] + map[1][0] + map[2][0],
                map[0][1] + map[1][1] + map[2][1], map[0][2] + map[1][2] + map[2][2],
                map[0][0] + map[1][1] + map[2][2], map[0][2] + map[1][1] + map[2][0]]

        if any(c == -3 for c in sums):
            draw_winner_line(sums.index(-3))
        elif any(c == 3 for c in sums):
            draw_winner_line(sums.index(3))

        return True


if __name__ == "__main__":
    pass

if __name__ == "console_game":
    game = TicTacGame()
    game.start_game(main_player=True, smart=True, depth=5)
    input("\nEnter any key...")

if __name__ == "gui_game":
    # Create executable: pyinstaller -F -w -n 'name'

    process = TicTacToe()
    root = TicTacGUI(process=process, with_AI=True).init()
    root.mainloop()
