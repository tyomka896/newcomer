import tkinter as tk
from tkinter import messagebox
from random import randrange
from math import floor, inf


class Logic:
    def __init__(self, web_syle=3):
        """
        Tic tac toe game class.
        There is initialization of the hollow game map (map)
        and the winner in final state (win_place).
        :param web_syle: 3x3 or 4x4 grid playground
        """
        self.web = web_syle
        self.depth = 5
        self.map = [[0 for _ in range(web_syle)] for _ in range(web_syle)]

    def make_throw(self, cell, tic_throw=True):
        """
        Make a throw as a gamer with passed row and column.
        :param cell: index of row and column as a throw
        :param tic_throw: who makes next throw tic (X) or tac (O)
        :return: True if throw successful and False if throw is not available or was already made
        """
        row, col = cell

        if 0 > row or row > (self.web - 1) or 0 > col or col > (self.web - 1):
            messagebox.Message(f"Range should be between 0 and {self.web - 1}")
            return False

        if self.map[row][col] != 0:
            messagebox.Message(f"The throw [{row + 1}, {col + 1}] already was made\n")
            return False

        self.map[row][col] = 1 if tic_throw else - 1

        return True

    def AI_random_throw(self):
        """
        AI makes its next throw by random
        It is simple logic without thinking
        :return: index of row and column of the chosen throw
        """
        available = self.available()

        rnd = randrange(0, len(available))
        row, col = available.pop(rnd)

        self.map[row][col] = -1

        return row, col

    def AI_smart_throw(self):
        """
        | AI makes its 'smart' next throw
        | based on Minimax algorithm
        """
        events = []
        natural = self.available()

        for ind, (row, col) in enumerate(natural):
            self.map[row][col] = -1
            # events.append((ind, self.__predict_throw(self.map, True, self.__depth)))
            events.append((ind, self.__predict_throw_prune(self.map, -inf, inf, True, self.depth)))
            self.map[row][col] = 0

        answer = events[0]
        for ind, score in events:
            if answer[1] > score:
                answer = (ind, score)

        answers = [(ind, score) for ind, score in events if score == answer[1]]
        answer = answers[randrange(0, len(answers))]

        row, col = natural.pop(answer[0])
        self.map[row][col] = -1

        return row, col

    def __predict_throw(self, status, is_maximize=True, depth=0):
        """
        Recursion function for finding the best next move for AI using Minimax algorithm
        :param status: current state or the map of the game
        :param is_maximize:  defines that we are trying to minimize or maximize our next throw
        :param depth: how deep in game tree we should descend
        :return: maximize or minimize value depend on is_maximize
        """
        winner = self.has_winner(status)
        if not winner == 0:
            return winner

        natural = self.available(status)
        if len(natural) == 0 or depth <= 0:
            return 0

        events = []

        if is_maximize:
            for row, col in natural:
                status[row][col] = 1
                events.append(self.__predict_throw(status, False, depth - 1))
                status[row][col] = 0

            return max(events)
        else:
            for row, col in natural:
                status[row][col] = -1
                events.append(self.__predict_throw(status, True, depth - 1))
                status[row][col] = 0

            return min(events)

    def __predict_throw_prune(self, status, alpha, beta, is_maximize=True, depth=0):
        """
        Recursion function for finding the best next move for AI using Minimax algorithm with Pruning
        :param status: current state or the map of the game
        :param alpha: maximum value for all predicted moves
        :param beta: minimum value for all predicted moves
        :param is_maximize:  defines that we are trying to minimize or maximize our next throw
        :param depth: how deep in game tree we should descend
        :return: maximize or minimize value depend on is_maximize
        """
        winner = self.has_winner(status)
        if not winner == 0:
            return winner

        natural = self.available(status)
        if len(natural) == 0 or depth <= 0:
            return 0

        events = []

        if is_maximize:
            for row, col in natural:
                status[row][col] = 1
                events.append(self.__predict_throw_prune(status, alpha, beta, False, depth - 1))
                status[row][col] = 0

                alpha = max(alpha, events[-1])
                if alpha >= beta:
                    break

            return max(events)
        else:
            for row, col in natural:
                status[row][col] = -1
                events.append(self.__predict_throw_prune(status, alpha, beta, True, depth - 1))
                status[row][col] = 0

                beta = min(beta, events[-1])
                if beta <= alpha:
                    break

            return min(events)

    def has_winner(self, map=None):
        """
        Function find a winner in a passed map (map)
        :param map: find a winner in a passed map
        :return: 1 if X (tic) is winner, -1 one if O (tac) is winner or 0 (toe) if nobody wins.
        """
        if not map:
            map = self.map

        results = []

        if self.web == 3:
            results = self.winner_three(map)
        elif self.web == 4:
            results = self.winner_four(map)

        if any(c == -self.web for c in results):
            return -1
        elif any(c == self.web for c in results):
            return 1
        else:
            return 0

    def available(self, map=None):
        """
        :param map: current state or map of the game
        :return: available moves in current map
        """
        if not map:
            map = self.map

        natural = []
        for ir, row in enumerate(map):
            for ic, col in enumerate(row):
                if col == 0:
                    natural.append((ir, ic))

        return natural

    def winner_three(self, map):
        """
        :param map:  current state or map of the game
        :return: state of the 3x3 grid playground
        """
        return [map[0][0] + map[0][1] + map[0][2], map[1][0] + map[1][1] + map[1][2],
                map[2][0] + map[2][1] + map[2][2], map[0][0] + map[1][0] + map[2][0],
                map[0][1] + map[1][1] + map[2][1], map[0][2] + map[1][2] + map[2][2],
                map[0][0] + map[1][1] + map[2][2], map[0][2] + map[1][1] + map[2][0]]

    def winner_four(self, map):
        """
        :param map:  current state or map of the game
        :return: state of the 4x4 grid playground
        """
        return [map[0][0] + map[0][1] + map[0][2] + map[0][3], map[1][0] + map[1][1] + map[1][2] + map[1][3],
                map[2][0] + map[2][1] + map[2][2] + map[2][3], map[0][0] + map[1][0] + map[2][0] + map[3][0],
                map[0][1] + map[1][1] + map[2][1] + map[3][1], map[0][2] + map[1][2] + map[2][2] + map[3][2],
                map[0][0] + map[1][1] + map[2][2] + map[3][3], map[0][3] + map[1][2] + map[2][1] + map[3][0],
                map[3][0] + map[3][1] + map[3][2] + map[3][3], map[0][3] + map[1][3] + map[2][3] + map[3][3],]


class GUI:
    def __init__(self):
        """
        Basic parameters of the new form
        """
        self.__logic = None      # Logic of the game
        self.__tic_throw = True  # First move is tic's (X)
        self.__AI = True

        self.__form = tk.Tk()  # Main form
        self.__web = 0         # Grid division 3x3 (3) or 4x4 (4)

        self.__form_width = 500
        self.__form_height = 550

        self.__cnv = None                  # Object of the main canvas
        self.__cnv_wh = 500                # Width and height of the canvas
        self.__cnv_font = "Comic Sans MS"  # Font style of X (tic) and O (tac)
        self.__cnv_indent = 0              # Indent of symbol placement
        self.__cnv_bg = "white"            # Background color of the canvas
        self.__cnv_strip_color = "black"   # Line color of the game board
        self.__cnv_strip_wth = 3           # Line width of the game board
        self.__cnv_cursor = "left_ptr"     # Cursor style

        self.__btn_rival = None   # Button for contestant changing from AI to PvP and vice versa
        self.__btn_grid = None    # Button for grind changing from 3x3 to 4x4 and vice versa
        self.__lbl_status = None  # Label to show status of the game

    @classmethod
    def init(cls, AI=True, web_style=3):
        """
        Creating new controllers as canvas, buttons and label
        and placing it to the form
        :param AI: playing with AI or PvP mode
        :param web_style: Grid size 3x3 or 4x4
        :return: new created GUI class instance
        """
        gui = cls()

        if not isinstance(web_style, int):
            raise ValueError("Parameter web_style should be integer type")

        if web_style < 3 or web_style > 4:
            raise ValueError("Grid division could by only 3x3 or 4x4")

        gui.__web = web_style
        gui.__logic = Logic(gui.__web)
        gui.__cnv_indent = 80 / (gui.__web / 3)

        # Initialization of the new form
        gui.__form.title("Noughts and Crosses")
        gui.__form.resizable(False, False)
        gui.__form.configure(bg=gui.__cnv_bg)
        gui.__form.geometry(f"{gui.__form_width}x{gui.__form_height}+300+150")

        # Create canvas
        gui.__cnv = tk.Canvas(gui.__form, width=gui.__cnv_wh, height=gui.__cnv_wh,
                              background=gui.__cnv_bg, cursor=gui.__cnv_cursor, highlightthickness=0)

        # Setting view of main controls
        gui.__btn_rival = tk.Button(gui.__form, text="", height=1, bg="white", fg="gray", bd=0,
                                    font=(gui.__cnv_font, 16))
        gui.__btn_grid = tk.Button(gui.__form, text=f"4x4", height=1, bg="white", fg="gray", bd=0,
                                   font=(gui.__cnv_font, 16))
        gui.__lbl_status = tk.Label(text="", bg="white", fg="black", font=(gui.__cnv_font, 20))

        # Setting depends on playing with AI or PvP
        gui.AI = AI

        if AI:
            gui.__cnv.bind("<Button-1>", gui.cnv_click_with_ai)
            gui.__btn_rival.configure(text="PvP")
            gui.__lbl_status.configure(text="TIC THROW")
        else:
            gui.__cnv.bind("<Button-1>", gui.cnv_click_pvp)
            gui.__lbl_status.configure(text="TAC THROW")

        gui.__cnv.bind("<Button-3>", gui.clear_canvas)
        gui.__btn_rival.bind("<Button-1>", gui.change_mode)
        gui.__btn_grid.bind("<Button-1>", gui.change_grid)

        # Placement of main controls
        gui.__cnv.grid(row=0, column=0)
        gui.__lbl_status.grid(row=1, column=0)
        gui.__btn_rival.place(x=gui.__cnv_wh - 60, y=gui.__cnv_wh)
        gui.__btn_grid.place(x=10, y=gui.__cnv_wh)

        return gui

    def start(self):
        """
        Clears and starts created form
        :return: self
        """
        self.clear_canvas()
        self.__form.mainloop()

        return self

    def clear_canvas(self, event=None):
        """
        Clears canvas
        :param event: uses if called from form event
        :return: self
        """
        self.__logic = Logic(self.__web)

        self.__tic_throw = True
        self.__lbl_status.configure(text="TIC THROW", fg="black")
        self.__cnv.delete("all")

        part = self.__cnv_wh / self.__web

        for web in range(self.__web - 1):
            self.__cnv.create_line(part + part * web, 0, part + part * web, self.__cnv_wh, fill=self.__cnv_strip_color,
                                   width=self.__cnv_strip_wth)
            self.__cnv.create_line(0, part + part * web, self.__cnv_wh, part + part * web, fill=self.__cnv_strip_color,
                                   width=self.__cnv_strip_wth)

        return self

    def cnv_click_pvp(self, event=None):
        """
        Click on canvas in PvP mode
        Draws X or O on canvas bases on whom turn
        :param event: uses if called from form event
        :return: True or False based on status of the game
        """
        if not self.get_status():
            return False

        part = self.__cnv_wh / self.__web

        cell_x = floor(event.x / part)
        cell_y = floor(event.y / part)

        if self.__tic_throw:
            if not self.__logic.make_throw((cell_y, cell_x)):
                return False
            symbol = "X"
            move = "TAC"
        else:
            if not self.__logic.make_throw((cell_y, cell_x), False):
                return False
            symbol = "O"
            move = "TIC"

        self.__tic_throw = not self.__tic_throw

        pos_x, pos_y = self.__cnv_indent + part * cell_x, self.__cnv_indent + part * cell_y

        self.__cnv.create_text(pos_x, pos_y, font=(self.__cnv_font, 60), text=symbol, fill="black")
        self.__lbl_status.configure(text=f"{move} THROW", fg="black")

        return self.get_status()

    def cnv_click_with_ai(self, event=None):
        """
        Click on canvas playing with AI
        At first mekes player throw and then AI predicts its next throw
        :param event: uses if called from form event
        :return: True or False based on status of the game
        """
        if not self.get_status():
            return False

        part = self.__cnv_wh / self.__web

        cell_x = floor(event.x / part)
        cell_y = floor(event.y / part)

        pos_x, pos_y = self.__cnv_indent + part * cell_x, self.__cnv_indent + part * cell_y

        if not self.__logic.make_throw((cell_y, cell_x)):
            return False

        self.__cnv.create_text(pos_x, pos_y, font=(self.__cnv_font, 60), text="X", fill="black")
        self.__lbl_status.configure(text=f"TAC THROW", fg="black")

        if not self.get_status():
            return False

        # row, col = self.__logic.AI_random_throw()
        row, col = self.__logic.AI_smart_throw()

        pos_x, pos_y = self.__cnv_indent + part * col, self.__cnv_indent + part * row

        self.__cnv.create_text(pos_x, pos_y, font=(self.__cnv_font, 60), text="O", fill="black")
        self.__lbl_status.configure(text=f"TAC THROW", fg="black")

        return self.get_status()

    def get_status(self):
        """
        :return: True if the game over or there is no available moves and False in opposite
        """
        winner = self.__logic.has_winner()

        if winner == 1:
            self.__lbl_status.configure(text="TIC WON", fg="green")
            self.show_winner(color="green")
            return False
        elif winner == -1:
            self.__lbl_status.configure(text="TAC WON", fg="red")
            self.show_winner(color="red")
            return False

        if len(self.__logic.available()) == 0:
            self.__lbl_status.configure(text="TIE", fg="blue")
            return False

        return True

    def show_winner(self, color):
        """
        Shows a winner with drawing a line of win position
        :param color: color to draw a line of the winner
        :return: True
        """
        def draw_winner_line(pos):
            indent = 50
            part = self.__cnv_wh / self.__web

            if pos == 0:
                # FIRST ROW
                self.__cnv.create_line(indent, part / 2, self.__cnv_wh - indent, part / 2, fill=color, width=5)
            elif pos == 1:
                # SECOND ROW
                self.__cnv.create_line(indent, part * 2 - part / 2, self.__cnv_wh - indent, part * 2 - part / 2, fill=color, width=5)
            elif pos == 2:
                # THIRD ROW
                self.__cnv.create_line(indent, part * 3 - part / 2, self.__cnv_wh - indent, part * 3 - part / 2, fill=color, width=5)
            elif pos == 8:
                # FORTH ROW (4x4)
                self.__cnv.create_line(indent, part * 4 - part / 2, self.__cnv_wh - indent, part * 4 - part / 2, fill=color, width=5)
            elif pos == 3:
                # FIRST COLUMN
                self.__cnv.create_line(part / 2, indent, part / 2, self.__cnv_wh - indent, fill=color, width=5)
            elif pos == 4:
                # SECOND COLUMN
                self.__cnv.create_line(part * 2 - part / 2, indent, part * 2 - part / 2, self.__cnv_wh - indent, fill=color, width=5)
            elif pos == 5:
                # THIRD COLUMN
                self.__cnv.create_line(part * 3 - part / 2, indent, part * 3 - part / 2, self.__cnv_wh - indent, fill=color, width=5)
            elif pos == 9:
                # FORTH COLUMN (4x4)
                self.__cnv.create_line(part * 4 - part / 2, indent, part * 4 - part / 2, self.__cnv_wh - indent, fill=color, width=5)
            elif pos == 6:
                # BACKSLASH
                self.__cnv.create_line(part / 2, part / 2, part * self.__web - part / 2, self.__cnv_wh - part / 2, fill=color, width=5)
            elif pos == 7:
                # FORWARD SLASH
                self.__cnv.create_line(part * self.__web - part / 2, part / 2, part / 2, self.__cnv_wh - part / 2, fill=color, width=5)

        results = []

        if self.__web == 3:
            results = self.__logic.winner_three(self.__logic.map)
        elif self.__web == 4:
            results = self.__logic.winner_four(self.__logic.map)

        if any(c == -self.__web for c in results):
            draw_winner_line(results.index(-self.__web))
        elif any(c == self.__web for c in results):
            draw_winner_line(results.index(self.__web))

        return True

    def change_mode(self, event=None):
        """
        Changes mode from playing with AI to PvP mode and vice versa
        :param event: uses if called from form event
        """
        self.__logic = Logic()
        self.__lbl_status.configure(text="TIC THROW", fg="black")
        self.clear_canvas()

        if self.__AI:
            self.__btn_rival.configure(text="AI")
            self.__cnv.unbind("<Button-1>")
            self.__cnv.bind("<Button-1>", self.cnv_click_pvp)
        else:
            self.__btn_rival.configure(text="PvP")
            self.__cnv.unbind("<Button-1>")
            self.__cnv.bind("<Button-1>", self.cnv_click_with_ai)

        self.__AI = not self.__AI

    def change_grid(self, event=None):
        """
        Changes grid from playing 3x3 to 4x4 mode and vice versa
        :param event: uses if called from form event
        """
        self.__lbl_status.configure(text="TIC THROW", fg="black")

        if self.__web == 3:
            self.__web = 4
            self.__btn_grid.configure(text="3x3")
        else:
            self.__web = 3
            self.__btn_grid.configure(text="4x4")

        self.__logic = Logic(web_syle=self.__web)
        self.__cnv_indent = 80 / (self.__web / 3)
        self.clear_canvas()


if __name__ == "__main__":
    # Create executable: pyinstaller -F -w script_name.py
    GUI.init(AI=True, web_style=3).start()
